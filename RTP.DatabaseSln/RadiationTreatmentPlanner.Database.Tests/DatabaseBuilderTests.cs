﻿using System.IO;
using NUnit.Framework;
using RadiationTreatmentPlanner.Database.SQLite;

namespace RadiationTreatmentPlanner.Database.Tests
{
    [TestFixture]
    class DatabaseBuilderTests
    {
        [Test]
        public void Build_Test()
        {
            Assert.DoesNotThrow(() => DatabaseBuilder.Initialize()
                .Build(new FileInfo("test-database.db")));
        }
    }
}
