﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Database.SQLite;
using System.IO;
using RadiationTreatmentPlanner.Database.Interface;
using RadiationTreatmentPlanner.Database.SQLite.Serialization;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Fields;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;
using RadiationTreatmentPlanner.Utils.Patient;
using RadiationTreatmentPlanner.Utils.Volume;
using System.Collections.Generic;
using System.Windows.Media.Media3D;
using System;
using System.Linq;
using RadiationTreatmentPlanner.Optimization;
using RadiationTreatmentPlanner.Utils.ROIType;
using RTP.CP;
using TcpNtcpCalculation.Immutables;

namespace RadiationTreatmentPlanner.Database.Tests
{
    [TestFixture]
    public class DatabaseManagerTests
    {
        [Test]
        public void CopyLeftToRight_ReturnFalseIf_Db1IsNull_Test()
        {
            var (db1, db2) = InitializeDatabases("db1-manager-db1-null.db", "db2-manager-db1-null.db");
            var dbManager = new DatabaseManager();
            var result = dbManager.CopyLeftToRight(null, db2);
            Assert.IsFalse(result.Success);
        }

        [Test]
        public void CopyLeftToRight_ReturnFalseIf_Db2IsNull_Test()
        {
            var (db1, db2) = InitializeDatabases("db1-manager-db2-null.db", "db2-manager-db2-null.db");
            var dbManager = new DatabaseManager();
            var result = dbManager.CopyLeftToRight(db1, null);
            Assert.IsFalse(result.Success);
        }

        [Test]
        public void CopyLeftToRight_ReturnFalseIf_Db1IsNotConnected_Test()
        {

            var (db1, db2) = InitializeDatabases("db1-manager-db1-not-connected.db", "db2-manager-db1-not-connected.db");
            var dbManager = new DatabaseManager();
            db1.CloseConnection();
            var result = dbManager.CopyLeftToRight(db1, db2);
            Assert.IsFalse(result.Success);
        }

        [Test]
        public void CopyLeftToRight_ReturnFalseIf_Db2IsNotConnected_Test()
        {
            var (db1, db2) = InitializeDatabases("db1-manager-db2-not-connected.db", "db2-manager-db2-not-connected.db");
            var dbManager = new DatabaseManager();
            db2.CloseConnection();
            var result = dbManager.CopyLeftToRight(db1, db2);
            Assert.IsFalse(result.Success);
        }

        [Test]
        public void CopyLeftToRight_Test()
        {
            var (db1, db2) = InitializeDatabases("db1-manager.db", "db2-manager.db");
            db1.OpenConnection();
            db2.OpenConnection();
            var dbManager = new DatabaseManager();
            var result = dbManager.CopyLeftToRight(db1, db2);
            Assert.IsTrue(result.Success);

            var optimizationSummariesResult = db2.GetAllOptimizationSummaries();
            Assert.Contains(new RtpOptimizationSummary("OS1", "NA", ""), optimizationSummariesResult);
            Assert.Contains(new RtpOptimizationSummary("OS2", "NA", ""), optimizationSummariesResult);
            Assert.AreEqual(2, optimizationSummariesResult.Count);

            var patientIdsResult = db2.GetIdsOfAvailablePatients().ToList();
            Assert.Contains("P1", patientIdsResult);
            Assert.Contains("P2", patientIdsResult);
            Assert.AreEqual(2, patientIdsResult.Count);

            var courseIdsP1Result = db2.GetIdsOfAvailableCourses("P1").ToList();
            Assert.Contains("C1", courseIdsP1Result);
            Assert.Contains("C2", courseIdsP1Result);
            Assert.Contains("C3", courseIdsP1Result);
            Assert.AreEqual(3, courseIdsP1Result.Count);

            var courseIdsP2Result = db2.GetIdsOfAvailableCourses("P2").ToList();
            Assert.Contains("C1", courseIdsP2Result);
            Assert.AreEqual(1, courseIdsP2Result.Count);

            var planSetupP1C1Result = db2.GetAllPlanSetups("P1", "C1").Select(x => x.Id).ToList();
            Assert.Contains("Plan1", planSetupP1C1Result);
            Assert.Contains("Plan2", planSetupP1C1Result);
            Assert.AreEqual(2, planSetupP1C1Result.Count);

            var planSetupP1C2Result = db2.GetAllPlanSetups("P1", "C2").Select(x => x.Id).ToList();
            Assert.Contains("Plan3", planSetupP1C2Result);
            Assert.AreEqual(1, planSetupP1C2Result.Count);

            var planSetupP1C3Result = db2.GetAllPlanSetups("P1", "C3").Select(x => x.Id).ToList();
            Assert.Contains("Plan4", planSetupP1C3Result);
            Assert.AreEqual(1, planSetupP1C3Result.Count);

            var optimizationResultsOs1PlanIDs = db2.GetOptimizationResultsOfOptimizationSummaryWithId("OS1")
                .Select(x => x.PlanSetupId).ToList();
            Assert.Contains("Plan1", optimizationResultsOs1PlanIDs);
            Assert.Contains("Plan2", optimizationResultsOs1PlanIDs);
            Assert.Contains("Plan3", optimizationResultsOs1PlanIDs);
            Assert.Contains("Plan4", optimizationResultsOs1PlanIDs);
            Assert.AreEqual(4, optimizationResultsOs1PlanIDs.Count);

            var optimizationResultsOs2Ids = db2.GetOptimizationResultsOfOptimizationSummaryWithId("OS2")
                .Select(x => x.PlanSetupId).ToList();
            Assert.Contains("Plan1", optimizationResultsOs2Ids);
            Assert.Contains("Plan2", optimizationResultsOs2Ids);
            Assert.AreEqual(2, optimizationResultsOs2Ids.Count);
        }
        public (IRtpDatabase db1, IRtpDatabase db2) InitializeDatabases(string db1FullFilePath, string db2FullFilePath)
        {
            var db1FileInfo = new FileInfo(db1FullFilePath);
            var db2FileInfo = new FileInfo(db2FullFilePath);
            db1FileInfo.Delete();
            db2FileInfo.Delete();
            db1FileInfo = new FileInfo(db1FullFilePath);
            db2FileInfo = new FileInfo(db2FullFilePath);

            IRtpDatabase db1 = new RtpSqliteDatabase(db1FileInfo, new RtpSerializer());
            IRtpDatabase db2 = new RtpSqliteDatabase(db2FileInfo, new RtpSerializer());

            db1.OpenConnection();
            db2.OpenConnection();

            db1.Initialize();
            db2.Initialize();
            
            // DB1.
            db1.AddPatient("P1", "P1");
            db1.AddPatient("P2", "P2");

            db1.AddStructureSet("SS1", "P1");
            db1.AddStructureSet("SS2", "P1");
            db1.AddStructureSet("SS1", "P2");

            db1.AddCourse("C1", "P1");
            db1.AddCourse("C2", "P1");

            db1.AddCourse("C1", "P2");

            db1.AddPlanSetup(CreatePlanSetup("Plan1", "C1", "SS1", "P1"));
            db1.AddPlanSetup(CreatePlanSetup("Plan2", "C1", "SS1", "P1"));
            db1.AddPlanSetup(CreatePlanSetup("Plan3", "C2", "SS2", "P1"));
            db1.AddPlanSetup(CreatePlanSetup("Plan1", "C1", "SS1", "P2"));
            db1.AddPlanSetup(CreatePlanSetup("Plan2", "C1", "SS1", "P2"));

            db1.AddOptimizationSummary(new RtpOptimizationSummary("OS1", "NA", ""));
            db1.AddOptimizationSummary(new RtpOptimizationSummary("OS2", "NA", ""));

            db1.AddOptimizationResult(
                CreateOptimizationRunResult("P1", "C1", "Plan1", "SS1", "NA", Option.None<UDose>(), 20), "OS1");
            db1.AddOptimizationResult(
                CreateOptimizationRunResult("P1", "C1", "Plan2", "SS1", "NA", Option.None<UDose>(), 20), "OS1");
            db1.AddOptimizationResult(
                CreateOptimizationRunResult("P1", "C2", "Plan3", "SS2", "NA", Option.None<UDose>(), 20), "OS1");
            var r = db1.AddOptimizationResult(
                CreateOptimizationRunResult("P2", "C1", "Plan1", "SS1", "NA", Option.None<UDose>(), 20), "OS2");
            var s = db1.AddOptimizationResult(
                CreateOptimizationRunResult("P2", "C1", "Plan2", "SS1", "NA", Option.None<UDose>(), 20), "OS2");

            // DB2.
            db2.AddPatient("P1", "P1");
            db2.AddStructureSet("SS3", "P1");
            db2.AddCourse("C3", "P1");
            db2.AddPlanSetup(CreatePlanSetup("Plan4", "C3", "SS3", "P1"));
            db2.AddOptimizationSummary(new RtpOptimizationSummary("OS1", "NA", ""));
            db2.AddOptimizationResult(
                CreateOptimizationRunResult("P1", "C3", "Plan4", "SS3", "NA", Option.None<UDose>(), 10), "OS1");

            return (db1, db2);
        }

        private UPlanSetup CreatePlanSetup(string planSetupId, string courseId, string structureSetId, string patientId)
        {
            var patient = new UPatient(patientId, "name");
            var structureSet = patient.AddStructureSetWithId(structureSetId);
            structureSet.AddStructureWithId("S1", new Option<(double, double, double)>(), new Option<string>(),
                new Option<double>(), new Option<bool>(), new Option<MeshGeometry3D>());
            structureSet.AddStructureWithId("S2", new Option<(double, double, double)>(), new Option<string>(),
                new Option<double>(), new Option<bool>(), new Option<MeshGeometry3D>());
            structureSet.AddStructureWithId("S3", new Option<(double, double, double)>(), new Option<string>(),
                new Option<double>(), new Option<bool>(), new Option<MeshGeometry3D>());

            var course = patient.AddCourseWithId(courseId);
            var planSetup = course.AddPlanSetupWithId(planSetupId, Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy),
                structureSet);
            planSetup.AddVMATArcRange(
                new List<VMATArc>
                {
                    new VMATArc("ARC1",
                        new GantryRotation(new AngleInDegree(179), new AngleInDegree(181), RotationDirection.Clockwise),
                        new AngleInDegree(10), new AngleInDegree(0)),
                    new VMATArc("ARC2",
                        new GantryRotation(new AngleInDegree(181), new AngleInDegree(179),
                            RotationDirection.AntiClockwise), new AngleInDegree(10), new AngleInDegree(0)),
                });
            planSetup.AddOptimizationObjectiveRange(new List<IObjective>
            {
                new PointObjective("S1", ObjectiveOperator.LOWER, new Priority(400), new UDose(50, UDose.UDoseUnit.Gy),
                    new UVolume(80, UVolume.VolumeUnit.percent)),
                new PointObjective("S1", ObjectiveOperator.UPPER, new Priority(300), new UDose(90, UDose.UDoseUnit.Gy),
                    new UVolume(2, UVolume.VolumeUnit.percent)),
                new EUDObjective("S2", ObjectiveOperator.LOWER, new Priority(100), new UDose(10, UDose.UDoseUnit.Gy),
                    -20),
                new EUDObjective("S2", ObjectiveOperator.UPPER, new Priority(1000), new UDose(90, UDose.UDoseUnit.Gy),
                    40),
                new MeanDoseObjective("S3", new UDose(20, UDose.UDoseUnit.Gy), new Priority(300))
            });
            planSetup.AddDvhEstimate("S1", 1, new UDose(10, UDose.UDoseUnit.Gy), new UDose(20, UDose.UDoseUnit.Gy),
                new UDose(20, UDose.UDoseUnit.Gy), new UDose(30, UDose.UDoseUnit.Gy), 1,
                new UDose(2, UDose.UDoseUnit.Gy), new UVolume(3, UVolume.VolumeUnit.ccm),
                new DVHCurve(
                    new List<Tuple<UDose, UVolume>>
                    {
                        new Tuple<UDose, UVolume>(new UDose(1, UDose.UDoseUnit.Gy),
                            new UVolume(1, UVolume.VolumeUnit.ccm)),
                        new Tuple<UDose, UVolume>(new UDose(1, UDose.UDoseUnit.Gy),
                            new UVolume(1, UVolume.VolumeUnit.ccm)),
                        new Tuple<UDose, UVolume>(new UDose(1, UDose.UDoseUnit.Gy),
                            new UVolume(1, UVolume.VolumeUnit.ccm))
                    }, DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.PHYSICAL, new UVolume(3, UVolume.VolumeUnit.ccm)));
            return planSetup;
        }

        private RtpOptimizationRunResult CreateOptimizationRunResult(string patientId, string courseId, string planSetupId,
            string structureSetId, string description, Option<UDose> totalPrescriptionDose, uint numberOfFractions)
        {
            var probabilityOfCureCalculationResult = new ProbabilityOfCureCalculationResult(new List<TcpPoint>
            {
                new TcpPoint("S1", new TumorControlProbability(0.78)),
                new TcpPoint("S2", new TumorControlProbability(0.83)),
            }, new ProbabilityOfCure(0.6474));
            var probabilityOfInjuryCalculationResult = new ProbabilityOfInjuryCalculationResult(new List<NtcpPoint>
            {
                new NtcpPoint("S3", new NormalTissueComplicationProbability(0.14), 1),
                new NtcpPoint("S4", new NormalTissueComplicationProbability(0.12), 1)
            }, new ProbabilityOfInjury(0.2432));
            return new RtpOptimizationRunResult(patientId, courseId, planSetupId, structureSetId, description,
                totalPrescriptionDose, numberOfFractions, probabilityOfCureCalculationResult,
                probabilityOfInjuryCalculationResult, new ComplicationFreeTumorControl(0.84255232), Option.Some(
                    new ClinicalProtocolEvaluationResult(new List<ClinicalProtocolItemEvaluationResult>
                    {
                        new ClinicalProtocolItemEvaluationResult(true,
                            new MinimumClinicalProtocolItem(new UDose(50, UDose.UDoseUnit.Gy),
                                new UVolume(98, UVolume.VolumeUnit.percent),
                                new RtRoiInterpretedType("S1", "S1 description"), ConstraintType.Hard),
                            new UDose(60, UDose.UDoseUnit.Gy)),
                        new ClinicalProtocolItemEvaluationResult(true,
                            new MinimumDoseClinicalProtocolItem(new UDose(49, UDose.UDoseUnit.Gy),
                                new RtRoiInterpretedType("S1", "S1 description"), ConstraintType.Hard),
                            new UDose(49, UDose.UDoseUnit.Gy)),
                        new ClinicalProtocolItemEvaluationResult(true,
                            new MaximumClinicalProtocolItem(new UDose(40, UDose.UDoseUnit.Gy),
                                new UVolume(98, UVolume.VolumeUnit.percent),
                                new RtRoiInterpretedType("S3", "S3 description"), ConstraintType.Hard),
                            new UDose(38, UDose.UDoseUnit.Gy)),
                        new ClinicalProtocolItemEvaluationResult(true,
                            new MaximumDoseClinicalProtocolItem(new UDose(40, UDose.UDoseUnit.Gy),
                                new RtRoiInterpretedType("S4", "S4 description"), ConstraintType.Hard),
                            new UDose(39, UDose.UDoseUnit.Gy)),
                    })), DateTime.Now, "");
        }
    }
}
