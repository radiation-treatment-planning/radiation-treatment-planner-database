﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media.Media3D;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Database.Interface;
using RadiationTreatmentPlanner.Database.SQLite;
using RadiationTreatmentPlanner.Database.SQLite.Serialization;
using RadiationTreatmentPlanner.Optimization;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Fields;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;
using RadiationTreatmentPlanner.Utils.Patient;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;
using RTP.CP;
using TcpNtcpCalculation.Immutables;

namespace RadiationTreatmentPlanner.Database.Tests
{
    [TestFixture]
    public class RtpPatientSqliteDatabaseTests
    {
        private FileInfo _dbFileInfo;
        private FileInfo _dbFileInfoCascade;
        private FileInfo _deletePatientDbFileInfo;
        private FileInfo _getAvailablePatientsDbFileInfo;
        private FileInfo _getIdsOfAvailableStructureSetsEmptyDbFileInfo;
        private FileInfo _getIdsOfAvailableCoursesDbFileInfo;
        private FileInfo _getIdsOfAvailableCoursesEmptyDbFileInfo;
        private FileInfo _getIdsOfAvailableCoursesNoPatientDbFileInfo;
        private FileInfo _optimizationSummariesDbFileInfo;
        private FileInfo _getOptimizationResultsDbFileInfo;
        private FileInfo _noOptimizationResultsDbFileInfo;
        private FileInfo _deleteOptimizationSummaryDbFileInfo;
        private FileInfo _dbFileInfoGetAllPlanSetups;

        [SetUp]
        public void SetUp()
        {
            _dbFileInfo = new FileInfo("test.db");
            _dbFileInfoGetAllPlanSetups = new FileInfo("test-get-all-plan-setups.db");
            _dbFileInfoCascade = new FileInfo("test-cascade.db");
            _deletePatientDbFileInfo = new FileInfo("delete-patient.db");
            _getIdsOfAvailableStructureSetsEmptyDbFileInfo = new FileInfo("GetIdsOfAvailableStructureSets_empty.db");
            _getAvailablePatientsDbFileInfo = new FileInfo("GetAvailablePatientIds.db");
            _getIdsOfAvailableCoursesDbFileInfo = new FileInfo("GetIdsOfAvailableCourses.db");
            _getIdsOfAvailableCoursesEmptyDbFileInfo = new FileInfo("GetIdsOfAvailableCourses_empty.db");
            _getIdsOfAvailableCoursesNoPatientDbFileInfo = new FileInfo("GetIdsOfAvailableCourses_empty_noPatient.db");
            _optimizationSummariesDbFileInfo = new FileInfo("db-optimizationresultssummary.db");
            _getOptimizationResultsDbFileInfo = new FileInfo("get-optimization-result.db");
            _noOptimizationResultsDbFileInfo = new FileInfo("no-optimization-result.db");
            _deleteOptimizationSummaryDbFileInfo = new FileInfo("delete-optimizationsummary.db");

            //DeleteAllDbFiles(new FileInfo[]
            //{
            //    _dbFileInfo, _dbFileInfoCascade, _deletePatientDbFileInfo,
            //    _getIdsOfAvailableStructureSetsEmptyDbFileInfo,
            //    _getAvailablePatientsDbFileInfo, _getIdsOfAvailableCoursesDbFileInfo,
            //    _getIdsOfAvailableCoursesEmptyDbFileInfo,
            //    _getIdsOfAvailableCoursesNoPatientDbFileInfo, _optimizationSummariesDbFileInfo,
            //    _getOptimizationResultsDbFileInfo,
            //    _noOptimizationResultsDbFileInfo, _deleteOptimizationSummaryDbFileInfo
            //});
        }

        private void DeleteAllDbFiles(FileInfo[] fileInfos)
        {
            foreach (var fileInfo in fileInfos)
            {
                fileInfo.Delete();
                fileInfo.Create();
            }
        }

        [Test]
        public void Constructor_CreateConnectionString_WithReadWriteCreateRights_Test()
        {
            var db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            Assert.IsTrue(db.ConnectionString.StartsWith("Data Source="));
            Assert.IsTrue(db.ConnectionString.Contains("Mode=ReadWriteCreate"));
        }

        [Test]
        public void OpenConnection_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            var result = db.OpenConnection();
            Assert.IsTrue(result.Success);
            Assert.IsEmpty(result.Info);
            Assert.IsTrue(db.IsDatabaseConnected);
        }

        [Test]
        public void CloseConnection_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            var result = db.CloseConnection();

            Assert.IsTrue(result.Success);
            Assert.IsEmpty(result.Info);
            Assert.IsFalse(db.IsDatabaseConnected);
        }

        [Test]
        public void CloseConnection_IfWasOpenedBefore_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            var result = db.CloseConnection();

            Assert.IsTrue(result.Success);
            Assert.IsEmpty(result.Info);
            Assert.IsFalse(db.IsDatabaseConnected);
        }

        [Test]
        public void Initialize_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            var result = db.Initialize();
            Assert.IsTrue(result.Success);
            db.CloseConnection();
        }

        [Test]
        public void AddPatient_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.DeletePatient("12345");
            var result = db.AddPatient("12345", "Max Musterfrau");
            var expectedInfo = "Added patient with ID 12345 and name Max Musterfrau.";
            Assert.IsTrue(result.Success);
            Assert.AreEqual(expectedInfo, result.Info);
            db.CloseConnection();
        }

        [Test]
        public void DeletePatient_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_deletePatientDbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("12345", "Max Musterfrau");
            db.AddStructureSet("S1", "12345");
            db.AddStructureSet("S2", "12345");
            db.AddCourse("C1", "12345");
            db.AddCourse("C2", "12345");
            var planSetup1 = CreatePlanSetup("P1", "C1", "S1", "12345");
            var planSetup2 = CreatePlanSetup("P2", "C1", "S2", "12345");
            var planSetup3 = CreatePlanSetup("P3", "C2", "S2", "12345");
            db.AddPlanSetup(planSetup1);
            db.AddPlanSetup(planSetup2);
            db.AddPlanSetup(planSetup3);


            var result = db.DeletePatient("12345");
            var structureSets = db.GetIdsOfAvailableStructureSets("12345");
            var courses = db.GetIdsOfAvailableCourses("12345");
            var planSetupResult1 = db.GetPlanSetupOrDefault("P1", "C1", "S1", "12345");
            var planSetupResult2 = db.GetPlanSetupOrDefault("P2", "C1", "S2", "12345");
            var planSetupResult3 = db.GetPlanSetupOrDefault("P3", "C2", "S2", "12345");

            var expectedInfo = "Deleted patient with ID 12345.";
            Assert.IsTrue(result.Success);
            Assert.AreEqual(expectedInfo, result.Info);
            Assert.IsEmpty(structureSets);
            Assert.IsEmpty(courses);
            Assert.IsNull(planSetupResult1);
            Assert.IsNull(planSetupResult2);
            Assert.IsNull(planSetupResult3);
            db.CloseConnection();
        }

        [Test]
        public void DeletePatient_OnDeletePatient_AlsoDeleteCoursesOfPatient_Test()
        {

            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfoCascade, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("On delete cascade", "Cascade");
            db.AddCourse("C1", "On delete cascade");
            db.AddCourse("C2", "On delete cascade");

            db.DeletePatient("On delete cascade");
            db.AddPatient("On delete cascade", "Cascade");

            var canAddFirstCourse = db.CanAddCourseToPatient("C1", "On delete cascade");
            var canAddSecondCourse = db.CanAddCourseToPatient("C2", "On delete cascade");

            Assert.IsTrue(canAddFirstCourse);
            Assert.IsTrue(canAddSecondCourse);
            db.CloseConnection();
        }

        [Test]
        public void GetPatientName_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("1", "Marie");
            db.AddPatient("2", "Max");

            Assert.AreEqual("Marie", db.GetPatientName("1"));
            Assert.AreEqual("Max", db.GetPatientName("2"));
        }

        [Test]
        public void GetPatientName_ReturnNullIfPatientDoesNotExist_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();

            Assert.IsNull(db.GetPatientName("34567"));
        }

        [Test]
        public void CanAddPatientWithId_ReturnTrueIfTrue_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            var patientId = "1234";

            db.OpenConnection();
            db.Initialize();
            db.DeletePatient(patientId);

            Assert.IsTrue(db.CanAddPatientWithId(patientId));
        }

        [Test]
        public void CanAddPatientWithId_ReturnFalseIfFalse_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            var patientId = "1234";

            db.OpenConnection();
            db.Initialize();
            db.AddPatient(patientId, "Max");

            Assert.IsFalse(db.CanAddPatientWithId(patientId));
        }

        [Test]
        public void GetAllAvailablePatientIds_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_getAvailablePatientsDbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("1", "Max");
            db.AddPatient("3", "Max");
            db.AddPatient("5", "Max");

            var result = db.GetIdsOfAvailablePatients().ToList();

            Assert.AreEqual(3, result.Count);
            Assert.Contains("1", result);
            Assert.Contains("3", result);
            Assert.Contains("5", result);
        }

        [Test]
        public void GetAllAvailablePatientIds_EmptyResult_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(new FileInfo("GetAvailablePatientIds_empty.db"), new RtpSerializer());

            db.OpenConnection();
            db.Initialize();

            var result = db.GetIdsOfAvailablePatients().ToList();

            Assert.IsEmpty(result);
        }

        [Test]
        public void AddStructureSet_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            var addPatientResult = db.AddPatient("111", "Marie");
            db.DeleteStructureSet("Structure Set 1", "111");
            var addStructureSetResult = db.AddStructureSet("Structure Set 1", "111");
            var expectedInfo = "Added structure set with ID Structure Set 1 for patient with ID 111.";

            Assert.IsTrue(addStructureSetResult.Success);
            Assert.AreEqual(expectedInfo, addStructureSetResult.Info);
            db.CloseConnection();
        }

        [Test]
        public void AddStructureSet_NoSuccess_IfPatientDoesNotExist_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.DeleteStructureSet("Structure Set 1", "666");
            var addStructureSetResult = db.AddStructureSet("Structure Set 1", "666");

            Assert.IsFalse(addStructureSetResult.Success);
            db.CloseConnection();
        }

        [Test]
        public void DeleteStructureSet_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("p1", "Marie");
            db.AddStructureSet("Structure Set 2", "p1");

            var deleteStructureSetResult = db.DeleteStructureSet("Structure Set 2", "p1");
            var expectedInfo = "Deleted structure set with ID Structure Set 2 for patient with ID p1.";

            Assert.IsTrue(deleteStructureSetResult.Success);
            Assert.AreEqual(expectedInfo, deleteStructureSetResult.Info);
            db.CloseConnection();
        }

        [Test]
        public void GetIdsOfAvailableStructureSets_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(new FileInfo("GetIdsOfAvailableStructureSets.db"), new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("444", "Alice");
            db.AddStructureSet("S1", "444");
            db.AddStructureSet("S3", "444");
            db.AddStructureSet("S5", "444");

            var result = db.GetIdsOfAvailableStructureSets("444").ToList();

            Assert.AreEqual(3, result.Count);
            Assert.Contains("S1", result);
            Assert.Contains("S3", result);
            Assert.Contains("S5", result);
            db.CloseConnection();
        }

        [Test]
        public void GetIdsOfAvailableStructureSets_EmptyResult_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_getIdsOfAvailableStructureSetsEmptyDbFileInfo,
                new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("444", "Alice");

            var result = db.GetIdsOfAvailableStructureSets("444").ToList();

            Assert.IsEmpty(result);
            db.CloseConnection();
        }

        [Test]
        public void AddCourse_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("222", "Martin");
            var deleteCourseResult = db.DeleteCourse("C1", "222");
            var addCourseResult = db.AddCourse("C1", "222");
            var expectedInfo = "Added course with ID C1 for patient with ID 222.";

            Assert.IsTrue(addCourseResult.Success);
            Assert.AreEqual(expectedInfo, addCourseResult.Info);
            db.CloseConnection();
        }

        [Test]
        public void AddCourse_NoSuccess_IfPatientDoesNotExist_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.DeletePatient("222");
            var addCourseResult = db.AddCourse("C1", "222");

            Assert.IsFalse(addCourseResult.Success);
            db.CloseConnection();
        }

        [Test]
        public void AddCourse_NoSuccess_IfCourseAlreadyExists_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("222", "Martin");
            db.AddCourse("C1", "222");
            var addCourseResult = db.AddCourse("C1", "222");

            Assert.IsFalse(addCourseResult.Success);
            db.CloseConnection();
        }

        [Test]
        public void CanAddCourseToPatient_ReturnFalse_IfPatientDoesNotExists_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.DeletePatient("3");
            var result = db.CanAddCourseToPatient("C2", "3");

            Assert.IsFalse(result);
            db.CloseConnection();
        }

        [Test]
        public void CanAddCourseToPatient_ReturnFalse_IfCourseAlreadyExists_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("333", "Alice");
            var deleteResult = db.DeleteCourse("C2", "333");
            var addCourseResult = db.AddCourse("C2", "333");
            var result = db.CanAddCourseToPatient("C2", "333");

            Assert.IsFalse(result);
            db.CloseConnection();
        }

        [Test]
        public void CanAddCourseToPatient_ReturnTrueIfTrue_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("444", "Alice");
            db.DeleteCourse("C2", "333");
            var result = db.CanAddCourseToPatient("C2", "444");

            Assert.IsTrue(result);
            db.CloseConnection();
        }

        [Test]
        public void GetIdsOfAvailableCourses_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_getIdsOfAvailableCoursesDbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("444", "Alice");
            db.AddCourse("C2", "444");
            db.AddCourse("C4", "444");
            db.AddCourse("C6", "444");

            var result = db.GetIdsOfAvailableCourses("444").ToList();

            Assert.AreEqual(3, result.Count);
            Assert.Contains("C2", result);
            Assert.Contains("C4", result);
            Assert.Contains("C6", result);
            db.CloseConnection();
        }

        [Test]
        public void GetIdsOfAvailableCourses_EmptyResult_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_getIdsOfAvailableCoursesEmptyDbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();
            db.AddPatient("444", "Alice");

            var result = db.GetIdsOfAvailableCourses("444").ToList();

            Assert.IsEmpty(result);
            db.CloseConnection();
        }

        [Test]
        public void GetIdsOfAvailableCourses_EmptyResult_PatientDoesNotExist_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_getIdsOfAvailableCoursesNoPatientDbFileInfo, new RtpSerializer());

            db.OpenConnection();
            db.Initialize();

            var result = db.GetIdsOfAvailableCourses("444").ToList();

            Assert.IsEmpty(result);
            db.CloseConnection();
        }

        [Test]
        public void AddPlanSetup_PlanSetupAsArgument_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            var planSetupId = "Plan with fractions and prescription";
            var courseId = "CX";
            var structureSetId = "SSP";
            var patientId = "444";

            db.OpenConnection();
            db.Initialize();
            var addPatientResult = db.AddPatient(patientId, "Bob");
            var addStructureSetResult = db.AddStructureSet(structureSetId, patientId);
            var addCourseResult = db.AddCourse(courseId, patientId);

            var deletePlanSetupResult = db.DeletePlanSetup(planSetupId, courseId, structureSetId, patientId);
            var planSetup = CreatePlanSetup(planSetupId, courseId, structureSetId, patientId);
            var addPlanSetupResult = db.AddPlanSetup(planSetup);
            var expectedInfo = $"Added Plan setup with ID {planSetupId} for patient with ID {patientId}, " +
                               $"course with ID {courseId} and structure set with ID {structureSetId}.";

            Assert.IsTrue(addPlanSetupResult.Success);
            Assert.AreEqual(expectedInfo, addPlanSetupResult.Info);
            db.CloseConnection();
        }

        [Test]
        public void AddPlanSetup_PlanSetupDetachedAsArgument_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            var planSetupId = "Plan with fractions and prescription";
            var courseId = "CX";
            var structureSetId = "SSP";
            var patientId = "444";

            db.OpenConnection();
            db.Initialize();
            var addPatientResult = db.AddPatient(patientId, "Bob");
            var addStructureSetResult = db.AddStructureSet(structureSetId, patientId);
            var addCourseResult = db.AddCourse(courseId, patientId);

            var deletePlanSetupResult = db.DeletePlanSetup(planSetupId, courseId, structureSetId, patientId);
            var planSetup = CreatePlanSetup(planSetupId, courseId, structureSetId, patientId);
            var planSetupDetached = planSetup.Detach();
            var addPlanSetupResult = db.AddPlanSetup(planSetupDetached, patientId, courseId);
            var expectedInfo = $"Added Plan setup with ID {planSetupId} for patient with ID {patientId}, " +
                               $"course with ID {courseId} and structure set with ID {structureSetId}.";

            Assert.IsTrue(addPlanSetupResult.Success);
            Assert.AreEqual(expectedInfo, addPlanSetupResult.Info);
            db.CloseConnection();
        }

        [Test]
        public void GetPlanSetupOrDefault_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            var planSetupId = "get plan setup";
            var courseId = "get plan course";
            var structureSetId = "get plan ss";
            var patientId = "get plan patient";

            db.OpenConnection();
            db.Initialize();
            var addPatientResult = db.AddPatient(patientId, "Bob");
            var addStructureSetResult = db.AddStructureSet(structureSetId, patientId);
            var addCourseResult = db.AddCourse(courseId, patientId);
            var planSetup = CreatePlanSetup(planSetupId, courseId, structureSetId, patientId);
            var deletePlanSetupResult = db.DeletePlanSetup(planSetup.Id, courseId, structureSetId, patientId);
            var addPlanSetupResult = db.AddPlanSetup(planSetup);

            var result = db.GetPlanSetupOrDefault(planSetupId, courseId, structureSetId, patientId);
            var expectedResult = planSetup.Detach();
            db.CloseConnection();

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void GetAllPlanSetups_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfoGetAllPlanSetups, new RtpSerializer());

            var planSetupId = "ps1";
            var planSetupId2 = "ps2";
            var courseId = "get plan course";
            var structureSetId = "get plan ss";
            var patientId = "get plan patient";

            db.OpenConnection();
            db.Initialize();
            var addPatientResult = db.AddPatient(patientId, "Bob");
            var addStructureSetResult = db.AddStructureSet(structureSetId, patientId);
            var addCourseResult = db.AddCourse(courseId, patientId);
            var planSetup = CreatePlanSetup(planSetupId, courseId, structureSetId, patientId);
            var planSetup2 = CreatePlanSetup(planSetupId2, courseId, structureSetId, patientId);
            var deletePlanSetupResult = db.DeletePlanSetup(planSetup.Id, courseId, structureSetId, patientId);
            var deletePlanSetup2Result = db.DeletePlanSetup(planSetup2.Id, courseId, structureSetId, patientId);

            var emptyPlanSetups = db.GetAllPlanSetups(patientId, courseId);
            Assert.IsEmpty(emptyPlanSetups);

            var addPlanSetupResult = db.AddPlanSetup(planSetup);
            var addPlanSetup2Result = db.AddPlanSetup(planSetup2);

            var result = db.GetAllPlanSetups(patientId, courseId);
            var expectedResult = new List<UPlanSetupDetached>() { planSetup.Detach(), planSetup2.Detach() };
            db.CloseConnection();

            Assert.IsTrue(expectedResult.SequenceEqual(result));
        }

        [Test]
        public void GetPlanSetupOrDefault_GetNullIfPlanSetupDoesNotExist_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            var planSetupId = "N/A";
            var courseId = "get plan course";
            var structureSetId = "get plan ss";
            var patientId = "get plan patient";

            db.OpenConnection();
            db.Initialize();
            var deletePlanSetupResult = db.DeletePlanSetup(planSetupId, courseId, structureSetId, patientId);

            var result = db.GetPlanSetupOrDefault(planSetupId, courseId, structureSetId, patientId);
            db.CloseConnection();

            Assert.IsNull(result);
        }

        private UPlanSetup CreatePlanSetup(string planSetupId, string courseId, string structureSetId, string patientId)
        {
            var patient = new UPatient(patientId, "name");
            var structureSet = patient.AddStructureSetWithId(structureSetId);
            structureSet.AddStructureWithId("S1", new Option<(double, double, double)>(), new Option<string>(),
                new Option<double>(), new Option<bool>(), new Option<MeshGeometry3D>());
            structureSet.AddStructureWithId("S2", new Option<(double, double, double)>(), new Option<string>(),
                new Option<double>(), new Option<bool>(), new Option<MeshGeometry3D>());
            structureSet.AddStructureWithId("S3", new Option<(double, double, double)>(), new Option<string>(),
                new Option<double>(), new Option<bool>(), new Option<MeshGeometry3D>());

            var course = patient.AddCourseWithId(courseId);
            var planSetup = course.AddPlanSetupWithId(planSetupId, Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy),
                structureSet);
            planSetup.AddVMATArcRange(
                new List<VMATArc>
                {
                    new VMATArc("ARC1",
                        new GantryRotation(new AngleInDegree(179), new AngleInDegree(181), RotationDirection.Clockwise),
                        new AngleInDegree(10), new AngleInDegree(0)),
                    new VMATArc("ARC2",
                        new GantryRotation(new AngleInDegree(181), new AngleInDegree(179),
                            RotationDirection.AntiClockwise), new AngleInDegree(10), new AngleInDegree(0)),
                });
            planSetup.AddOptimizationObjectiveRange(new List<IObjective>
            {
                new PointObjective("S1", ObjectiveOperator.LOWER, new Priority(400), new UDose(50, UDose.UDoseUnit.Gy),
                    new UVolume(80, UVolume.VolumeUnit.percent)),
                new PointObjective("S1", ObjectiveOperator.UPPER, new Priority(300), new UDose(90, UDose.UDoseUnit.Gy),
                    new UVolume(2, UVolume.VolumeUnit.percent)),
                new EUDObjective("S2", ObjectiveOperator.LOWER, new Priority(100), new UDose(10, UDose.UDoseUnit.Gy),
                    -20),
                new EUDObjective("S2", ObjectiveOperator.UPPER, new Priority(1000), new UDose(90, UDose.UDoseUnit.Gy),
                    40),
                new MeanDoseObjective("S3", new UDose(20, UDose.UDoseUnit.Gy), new Priority(300))
            });
            planSetup.AddDvhEstimate("S1", 1, new UDose(10, UDose.UDoseUnit.Gy), new UDose(20, UDose.UDoseUnit.Gy),
                new UDose(20, UDose.UDoseUnit.Gy), new UDose(30, UDose.UDoseUnit.Gy), 1,
                new UDose(2, UDose.UDoseUnit.Gy), new UVolume(3, UVolume.VolumeUnit.ccm),
                new DVHCurve(
                    new List<Tuple<UDose, UVolume>>
                    {
                        new Tuple<UDose, UVolume>(new UDose(1, UDose.UDoseUnit.Gy),
                            new UVolume(1, UVolume.VolumeUnit.ccm)),
                        new Tuple<UDose, UVolume>(new UDose(1, UDose.UDoseUnit.Gy),
                            new UVolume(1, UVolume.VolumeUnit.ccm)),
                        new Tuple<UDose, UVolume>(new UDose(1, UDose.UDoseUnit.Gy),
                            new UVolume(1, UVolume.VolumeUnit.ccm))
                    }, DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.PHYSICAL, new UVolume(3, UVolume.VolumeUnit.ccm)));
            return planSetup;
        }

        [Test]
        public void AddOptimizationSummary_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());
            var optimizationSummary = new RtpOptimizationSummary("os1", "Description", "Some Info");

            db.OpenConnection();
            db.Initialize();
            var deleteOptimizationSummaryResult = db.DeleteOptimizationSummaryWithId(optimizationSummary.Id);
            var addOptimizationSummaryResult = db.AddOptimizationSummary(optimizationSummary);
            var expectedInfo =
                $"Added optimization summary with ID {optimizationSummary.Id}.";

            Assert.IsTrue(addOptimizationSummaryResult.Success);
            Assert.AreEqual(expectedInfo, addOptimizationSummaryResult.Info);
            db.CloseConnection();
        }

        [Test]
        public void CanAddOptimizationSummary_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());
            var optimizationSummary = new RtpOptimizationSummary("os1", "Description", "Some Info");

            db.OpenConnection();
            db.Initialize();
            var deleteOptimizationSummaryResult = db.DeleteOptimizationSummaryWithId(optimizationSummary.Id);
            var result1 = db.CanAddOptimizationSummary(optimizationSummary);
            var addOptimizationSummaryResult = db.AddOptimizationSummary(optimizationSummary);
            var result2 = db.CanAddOptimizationSummary(optimizationSummary);

            Assert.IsTrue(result1);
            Assert.IsFalse(result2);
            db.CloseConnection();
        }

        [Test]
        public void GetOptimizationSummaryWithId_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());
            var optimizationSummary = new RtpOptimizationSummary("os1", "Description", "Some Info");

            db.OpenConnection();
            db.Initialize();
            var deleteOptimizationSummaryResult = db.DeleteOptimizationSummaryWithId(optimizationSummary.Id);
            var addOptimizationSummaryResult = db.AddOptimizationSummary(optimizationSummary);
            var result = db.GetOptimizationSummaryWithId(optimizationSummary.Id);

            Assert.AreEqual(optimizationSummary, result);
            db.CloseConnection();
        }

        [Test]
        public void GetAllOptimizationSummaries_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_optimizationSummariesDbFileInfo, new RtpSerializer());
            var tmpAdditionalInformation = "Some Info 1";
            var tmpAdditionalInformation2 = "Some Info 2";
            var optimizationSummary = new RtpOptimizationSummary("os1", "Description", tmpAdditionalInformation);
            var optimizationSummary2 = new RtpOptimizationSummary("os2", "Description2", tmpAdditionalInformation2);

            db.OpenConnection();
            db.Initialize();
            var deleteOptimizationSummaryResult = db.DeleteOptimizationSummaryWithId(optimizationSummary.Id);
            var deleteOptimizationSummaryResult2 = db.DeleteOptimizationSummaryWithId(optimizationSummary2.Id);
            var addOptimizationSummaryResult = db.AddOptimizationSummary(optimizationSummary);
            var addOptimizationSummaryResult2 = db.AddOptimizationSummary(optimizationSummary2);
            var result = db.GetAllOptimizationSummaries();

            Assert.AreEqual(2, result.Count);
            Assert.Contains(optimizationSummary, result);
            Assert.Contains(optimizationSummary2, result);
            db.CloseConnection();
        }

        [Test]
        public void AddOptimizationResult_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            var planSetupId = "Plan with optimization result";
            var courseId = "Course with optimization result";
            var structureSetId = "SS with optimization result";
            var patientId = "Patient with optimization result";
            var optimizationSummary = new RtpOptimizationSummary("os1", "Description", "N/A");

            db.OpenConnection();
            db.Initialize();
            var addPatientResult = db.AddPatient(patientId, "Alice");
            var addStructureSetResult = db.AddStructureSet(structureSetId, patientId);
            var addCourseResult = db.AddCourse(courseId, patientId);
            var planSetup = CreatePlanSetup(planSetupId, courseId, structureSetId, patientId);
            var addPlanSetupResult = db.AddPlanSetup(planSetup);
            var optimizationRunResult = CreateOptimizationRunResult(patientId, courseId, planSetupId, structureSetId,
                "An optimization description", Option.Some(new UDose(40, UDose.UDoseUnit.Gy)), 12);
            var deleteOptimizationSummaryResult = db.DeleteOptimizationSummaryWithId(optimizationSummary.Id);
            var addOptimizationSummaryResult = db.AddOptimizationSummary(optimizationSummary);
            var addOptimizationResult = db.AddOptimizationResult(optimizationRunResult, optimizationSummary.Id);
            var expectedInfo =
                $"Added optimization result for patient with ID {patientId}, course with ID {courseId}, " +
                $"plan setup with ID {planSetupId} and structure set with ID {structureSetId}.";

            Assert.IsTrue(addOptimizationResult.Success);
            Assert.AreEqual(expectedInfo, addOptimizationResult.Info);
            db.CloseConnection();
        }

        [Test]
        public void AddOptimizationResult_ThrowError_IfAccordingPlanSetupDoesNotExist_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            var planSetupId = "Plan with optimization result 2";
            var courseId = "Course with optimization result 2";
            var structureSetId = "SS with optimization result 2";
            var patientId = "Patient with optimization result 2";
            var optimizationSummary = new RtpOptimizationSummary("os1", "Description", "N/A");

            db.OpenConnection();
            db.Initialize();
            var addPatientResult = db.AddPatient(patientId, "Alice");
            var addStructureSetResult = db.AddStructureSet(structureSetId, patientId);
            var addCourseResult = db.AddCourse(courseId, patientId);
            var addOptimizationSummaryResult = db.AddOptimizationSummary(optimizationSummary);
            var optimizationRunResult = CreateOptimizationRunResult(patientId, courseId, planSetupId, structureSetId,
                "An optimization description", Option.Some(new UDose(40, UDose.UDoseUnit.Gy)), 12);

            var addOptimizationResult = db.AddOptimizationResult(optimizationRunResult, optimizationSummary.Id);

            Assert.IsFalse(addOptimizationResult.Success);
            db.CloseConnection();
        }

        [Test]
        public void GetOptimizationResultOrDefault_GetNullIfOptimizationResultDoesNotExist_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_noOptimizationResultsDbFileInfo, new RtpSerializer());

            var planSetupId = "Plan with optimization result 4";
            var courseId = "Course with optimization result 4";
            var structureSetId = "SS with optimization result 4";
            var patientId = "Patient with optimization result 4";

            db.OpenConnection();
            db.Initialize();
            var addPatientResult = db.AddPatient(patientId, "Alice");
            var addStructureSetResult = db.AddStructureSet(structureSetId, patientId);
            var addCourseResult = db.AddCourse(courseId, patientId);
            var planSetup = CreatePlanSetup(planSetupId, courseId, structureSetId, patientId);
            var addPlanSetupResult = db.AddPlanSetup(planSetup);
            var result = db.GetOptimizationResultOrDefault(10);

            Assert.IsNull(result);
            db.CloseConnection();
        }

        [Test]
        public void GetOptimizationResultsOfOptimizationSummaryWithId_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(_dbFileInfo, new RtpSerializer());

            var optimizationSummary = new RtpOptimizationSummary("os1", "Description", "N/A");
            var optimizationSummary2 = new RtpOptimizationSummary("os2", "Descriptio2", "N/A");

            var planSetupId = "Plan with optimization result 6";
            var planSetupId2 = "Plan with optimization result 6.2";
            var planSetupId3 = "Plan with optimization result 6.3";
            var courseId = "Course with optimization result 6";
            var structureSetId = "SS with optimization result 6";
            var patientId = "Patient with optimization result 6";

            db.OpenConnection();
            db.Initialize();
            var addPatientResult = db.AddPatient(patientId, "Alice");
            var addStructureSetResult = db.AddStructureSet(structureSetId, patientId);
            var addCourseResult = db.AddCourse(courseId, patientId);

            var planSetup = CreatePlanSetup(planSetupId, courseId, structureSetId, patientId);
            var addPlanSetupResult = db.AddPlanSetup(planSetup);
            var planSetup2 = CreatePlanSetup(planSetupId2, courseId, structureSetId, patientId);
            var addPlanSetupResult2 = db.AddPlanSetup(planSetup2);
            var planSetup3 = CreatePlanSetup(planSetupId3, courseId, structureSetId, patientId);
            var addPlanSetupResult3 = db.AddPlanSetup(planSetup3);

            var optimizationRunResult = CreateOptimizationRunResult(patientId, courseId, planSetupId, structureSetId,
                "An optimization description", Option.Some(new UDose(40, UDose.UDoseUnit.Gy)), 12);
            var optimizationRunResult2 = CreateOptimizationRunResult(patientId, courseId, planSetupId2, structureSetId,
                "An second optimization description", Option.Some(new UDose(44, UDose.UDoseUnit.Gy)), 15);
            var optimizationRunResult3 = CreateOptimizationRunResult(patientId, courseId, planSetupId3, structureSetId,
                "An third optimization description", Option.Some(new UDose(42, UDose.UDoseUnit.Gy)), 13);

            var deleteOptimizationSummary1 = db.DeleteOptimizationSummaryWithId(optimizationSummary.Id);
            var deleteOptimizationSummary2 = db.DeleteOptimizationSummaryWithId(optimizationSummary2.Id);
            var addOptimizationSummary1 = db.AddOptimizationSummary(optimizationSummary);
            var addOptimizationSummary2 = db.AddOptimizationSummary(optimizationSummary2);

            var addOptimizationResult = db.AddOptimizationResult(optimizationRunResult, optimizationSummary.Id);
            var addOptimizationResult2 = db.AddOptimizationResult(optimizationRunResult2, optimizationSummary.Id);
            var addOptimizationResult3 = db.AddOptimizationResult(optimizationRunResult3, optimizationSummary2.Id);

            var result = db.GetOptimizationResultsOfOptimizationSummaryWithId(optimizationSummary.Id);
            var result2 = db.GetOptimizationResultsOfOptimizationSummaryWithId(optimizationSummary2.Id);

            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result2.Count);

            Assert.Contains(optimizationRunResult, result);
            Assert.Contains(optimizationRunResult2, result);
            Assert.Contains(optimizationRunResult3, result2);
            db.CloseConnection();
        }

        [Test]
        public void DeleteOptimizationSummaryWithId_Test()
        {

            IRtpDatabase db = new RtpSqliteDatabase(_deleteOptimizationSummaryDbFileInfo, new RtpSerializer());

            var optimizationSummary = new RtpOptimizationSummary("os1", "Description", "N/A");
            var optimizationSummary2 = new RtpOptimizationSummary("os2", "Description2", "N/A");

            var planSetupId = "Plan with optimization result 6";
            var planSetupId2 = "Plan with optimization result 6.2";
            var planSetupId3 = "Plan with optimization result 6.3";
            var courseId = "Course with optimization result 6";
            var structureSetId = "SS with optimization result 6";
            var patientId = "Patient with optimization result 6";

            db.OpenConnection();
            db.Initialize();
            var addPatientResult = db.AddPatient(patientId, "Alice");
            var addStructureSetResult = db.AddStructureSet(structureSetId, patientId);
            var addCourseResult = db.AddCourse(courseId, patientId);

            var planSetup = CreatePlanSetup(planSetupId, courseId, structureSetId, patientId);
            var addPlanSetupResult = db.AddPlanSetup(planSetup);
            var planSetup2 = CreatePlanSetup(planSetupId2, courseId, structureSetId, patientId);
            var addPlanSetupResult2 = db.AddPlanSetup(planSetup2);
            var planSetup3 = CreatePlanSetup(planSetupId3, courseId, structureSetId, patientId);
            var addPlanSetupResult3 = db.AddPlanSetup(planSetup3);

            var optimizationRunResult = CreateOptimizationRunResult(patientId, courseId, planSetupId, structureSetId,
                "An optimization description", Option.Some(new UDose(40, UDose.UDoseUnit.Gy)), 12);
            var optimizationRunResult2 = CreateOptimizationRunResult(patientId, courseId, planSetupId2, structureSetId,
                "An second optimization description", Option.Some(new UDose(44, UDose.UDoseUnit.Gy)), 15);
            var optimizationRunResult3 = CreateOptimizationRunResult(patientId, courseId, planSetupId3, structureSetId,
                "An third optimization description", Option.Some(new UDose(42, UDose.UDoseUnit.Gy)), 13);

            var deleteOptimizationSummary1 = db.DeleteOptimizationSummaryWithId(optimizationSummary.Id);
            var deleteOptimizationSummary2 = db.DeleteOptimizationSummaryWithId(optimizationSummary2.Id);
            var addOptimizationSummary1 = db.AddOptimizationSummary(optimizationSummary);
            var addOptimizationSummary2 = db.AddOptimizationSummary(optimizationSummary2);

            var addOptimizationResult = db.AddOptimizationResult(optimizationRunResult, optimizationSummary.Id);
            var addOptimizationResult2 = db.AddOptimizationResult(optimizationRunResult2, optimizationSummary.Id);
            var addOptimizationResult3 = db.AddOptimizationResult(optimizationRunResult3, optimizationSummary2.Id);

            var result = db.DeleteOptimizationSummaryWithId(optimizationSummary.Id);
            Assert.IsTrue(result.Success);
            var resultSummaryIds = db.GetAllOptimizationSummaries();
            var resultPlanSetups = db.GetOptimizationResultsOfOptimizationSummaryWithId(optimizationSummary.Id);

            Assert.AreEqual(1, resultSummaryIds.Count);
            db.CloseConnection();
        }

        [Test]
        public void CanAddStructureSetWithId_Test()
        {
            IRtpDatabase db = new RtpSqliteDatabase(new FileInfo("can-add-structure-set.db"), new RtpSerializer());
            db.OpenConnection();
            db.Initialize();

            var patientId = "p1";
            var structureSetId = "S1";
            db.AddPatient(patientId, patientId);
            db.DeleteStructureSet(structureSetId, patientId);

            var positiveResult = db.CanAddStructureSetWithId(structureSetId, patientId);
            Assert.IsTrue(positiveResult);

            var addStructureSetResult = db.AddStructureSet(structureSetId, patientId);
            var negativeResult = db.CanAddStructureSetWithId(structureSetId, patientId);
            Assert.IsFalse(negativeResult);

            db.CloseConnection();
        }


        private RtpOptimizationRunResult CreateOptimizationRunResult(string patientId, string courseId, string planSetupId,
            string structureSetId, string description, Option<UDose> totalPrescriptionDose, uint numberOfFractions)
        {
            var probabilityOfCureCalculationResult = new ProbabilityOfCureCalculationResult(new List<TcpPoint>
            {
                new TcpPoint("S1", new TumorControlProbability(0.78)),
                new TcpPoint("S2", new TumorControlProbability(0.83)),
            }, new ProbabilityOfCure(0.6474));
            var probabilityOfInjuryCalculationResult = new ProbabilityOfInjuryCalculationResult(new List<NtcpPoint>
            {
                new NtcpPoint("S3", new NormalTissueComplicationProbability(0.14), 1),
                new NtcpPoint("S4", new NormalTissueComplicationProbability(0.12), 1)
            }, new ProbabilityOfInjury(0.2432));
            return new RtpOptimizationRunResult(patientId, courseId, planSetupId, structureSetId, description,
                totalPrescriptionDose, numberOfFractions, probabilityOfCureCalculationResult,
                probabilityOfInjuryCalculationResult, new ComplicationFreeTumorControl(0.84255232), Option.Some(
                    new ClinicalProtocolEvaluationResult(new List<ClinicalProtocolItemEvaluationResult>
                    {
                        new ClinicalProtocolItemEvaluationResult(true,
                            new MinimumClinicalProtocolItem(new UDose(50, UDose.UDoseUnit.Gy),
                                new UVolume(98, UVolume.VolumeUnit.percent),
                                new RtRoiInterpretedType("S1", "S1 description"), ConstraintType.Hard),
                            new UDose(60, UDose.UDoseUnit.Gy)),
                        new ClinicalProtocolItemEvaluationResult(true,
                            new MinimumDoseClinicalProtocolItem(new UDose(49, UDose.UDoseUnit.Gy),
                                new RtRoiInterpretedType("S1", "S1 description"), ConstraintType.Hard),
                            new UDose(49, UDose.UDoseUnit.Gy)),
                        new ClinicalProtocolItemEvaluationResult(true,
                            new MaximumClinicalProtocolItem(new UDose(40, UDose.UDoseUnit.Gy),
                                new UVolume(98, UVolume.VolumeUnit.percent),
                                new RtRoiInterpretedType("S3", "S3 description"), ConstraintType.Hard),
                            new UDose(38, UDose.UDoseUnit.Gy)),
                        new ClinicalProtocolItemEvaluationResult(true,
                            new MaximumDoseClinicalProtocolItem(new UDose(40, UDose.UDoseUnit.Gy),
                                new RtRoiInterpretedType("S4", "S4 description"), ConstraintType.Hard),
                            new UDose(39, UDose.UDoseUnit.Gy)),
                    })), DateTime.Now, null);
        }
    }
}
