﻿using System;
using System.Collections.Generic;
using System.IO;
using RadiationTreatmentPlanner.Database.Interface;
using RadiationTreatmentPlanner.Database.SQLite.Serialization;
using RadiationTreatmentPlanner.Utils.Patient;

namespace RadiationTreatmentPlanner.Database.SQLite
{
    public class DatabaseBuilder
    {
        private readonly List<UPatient> _patients;

        private DatabaseBuilder()
        {
            _patients = new List<UPatient>();
        }

        public static DatabaseBuilder Initialize()
        {
            return new DatabaseBuilder();
        }

        public void WithPatient(UPatient patient)
        {
            if (patient == null) throw new ArgumentNullException(nameof(patient));
            _patients.Add(patient);
        }

        public IRtpDatabase Build(FileInfo fullFilePath)
        {
            if (fullFilePath == null) throw new ArgumentNullException(nameof(fullFilePath));
            var database = new RtpSqliteDatabase(fullFilePath, new RtpSerializer());
            if (!database.IsDatabaseConnected) database.OpenConnection();
            database.Initialize();
            database.CloseConnection();
            return database;
        }
    }
}
