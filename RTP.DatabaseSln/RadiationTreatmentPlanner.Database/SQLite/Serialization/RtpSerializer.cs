﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using RadiationTreatmentPlanner.Optimization;
using RadiationTreatmentPlanner.Utils.Patient;

namespace RadiationTreatmentPlanner.Database.SQLite.Serialization
{
    public class RtpSerializer
    {
        public string Serialize(UPlanSetupDetached planSetup)
        {
            return JsonConvert.SerializeObject(planSetup, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                Converters = new List<JsonConverter> { new OptionConverter() },
                Culture = CultureInfo.InvariantCulture
            });
        }

        public UPlanSetupDetached DeserializePlanSetup(string serializedPlanSetup)
        {
            if (serializedPlanSetup == null) throw new ArgumentNullException(nameof(serializedPlanSetup));
            return JsonConvert.DeserializeObject<UPlanSetupDetached>(serializedPlanSetup,
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto,
                    Converters = new List<JsonConverter> { new OptionConverter() },
                    Culture = CultureInfo.InvariantCulture
                });
        }
        public string Serialize(RtpOptimizationRunResult optimizationRunResult)
        {
            return JsonConvert.SerializeObject(optimizationRunResult, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                Converters = new List<JsonConverter> { new OptionConverter() },
                Culture = CultureInfo.InvariantCulture
            });
        }

        public RtpOptimizationRunResult DeserializeOptimizationRunResult(string serializedOptimizationRunResult)
        {
            if (serializedOptimizationRunResult == null) throw new ArgumentNullException(nameof(serializedOptimizationRunResult));
            return JsonConvert.DeserializeObject<RtpOptimizationRunResult>(serializedOptimizationRunResult,
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto,
                    Converters = new List<JsonConverter> { new OptionConverter() },
                    Culture = CultureInfo.InvariantCulture
                });
        }
    }
}
