﻿using System;
using System.Collections.Generic;
using System.Linq;
using RadiationTreatmentPlanner.Database.Interface;

namespace RadiationTreatmentPlanner.Database.SQLite
{
    public class DatabaseManager : IDatabaseManager
    {
        public delegate void OnProgressChangedEventHandler(object sender, OnProgressChangedEventArgs e);

        public event OnProgressChangedEventHandler OnProgressChanged;

        /// <summary>
        /// Copies all data from <param name="db1">db1</param> to <param name="db2">db2</param> for
        /// which it is possible. In case of duplicates the copying will be skipped and not synced!
        /// </summary>
        /// <param name="db1">Source Database.</param>
        /// <param name="db2">Target Database.</param>
        /// <returns>DbCopyResult which contains information about the copy process.</returns>
        public DbCopyResult CopyLeftToRight(IRtpDatabase db1, IRtpDatabase db2)
        {
            if (db1 == null) return new DbCopyResult(false, $"{nameof(db1)} is null.");
            if (db2 == null) return new DbCopyResult(false, $"{nameof(db2)} is null.");
            if (!db1.IsDatabaseConnected) return new DbCopyResult(false, $"{nameof(db1)} is not connected.");
            if (!db2.IsDatabaseConnected) return new DbCopyResult(false, $"{nameof(db2)} is not connected.");

            var copyPatientsResult = CopyPatients(db1, db2);
            if (!copyPatientsResult.Success) return new DbCopyResult(false, copyPatientsResult.Info);

            var copyStructureSetsResult = CopyStructureSets(db1, db2);
            if (!copyStructureSetsResult.Success) return new DbCopyResult(false, copyStructureSetsResult.Info);

            var copyCoursesResult = CopyCourses(db1, db2);
            if (!copyCoursesResult.Success) return new DbCopyResult(false, copyCoursesResult.Info);

            var copyPlanSetupsResult = CopyPlanSetups(db1, db2);
            if (!copyPlanSetupsResult.Success) return new DbCopyResult(false, copyPlanSetupsResult.Info);

            var copyOptimizationSummariesResult = CopyOptimizationSummaries(db1, db2);
            if (!copyOptimizationSummariesResult.Success)
                return new DbCopyResult(false, copyOptimizationSummariesResult.Info);

            var copyOptimizationResultsResult = CopyOptimizationResults(db1, db2);
            if (!copyOptimizationResultsResult.Success)
                return new DbCopyResult(false, copyOptimizationResultsResult.Info);

            return new DbCopyResult(true, $"Successfully copied {nameof(db1)} to {nameof(db2)}.");
        }

        private CopyResult CopyOptimizationResults(IRtpDatabase db1, IRtpDatabase db2)
        {
            var optimizationResultsCount = 0;
            var optimizationSummaryIds = db1.GetAllOptimizationSummaries().Select(x =>x.Id).ToList();
            foreach (var optimizationSummaryId in optimizationSummaryIds)
            {
                var optimizationResults = db1.GetOptimizationResultsOfOptimizationSummaryWithId(optimizationSummaryId);
                foreach (var optimizationResult in optimizationResults)
                {
                    var result = db2.AddOptimizationResult(optimizationResult, optimizationSummaryId);
                    OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs(result.Success, result.Info));
                    if (!result.Success) return new CopyResult(false, result.Info);
                    optimizationResultsCount++;
                }
            }

            return new CopyResult(true, $"Successfully copied {optimizationResultsCount} optimization Results.");
        }

        private CopyResult CopyPlanSetups(IRtpDatabase db1, IRtpDatabase db2)
        {
            var patientIds = db1.GetIdsOfAvailablePatients();
            var planSetupCount = 0;
            foreach (var patientId in patientIds)
            {
                var courseIds = db1.GetIdsOfAvailableCourses(patientId);
                foreach (var courseId in courseIds)
                {
                    var planSetups = db1.GetAllPlanSetups(patientId, courseId);
                    foreach (var planSetup in planSetups)
                    {
                        var result = db2.AddPlanSetup(planSetup, patientId, courseId);
                        OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs(result.Success, result.Info));
                        if (!result.Success) return new CopyResult(false, result.Info);
                    }

                    planSetupCount *= planSetups.Count();
                }
            }

            return new CopyResult(true, $"Successfully copied {planSetupCount} plan setups.");
        }

        private CopyResult CopyCourses(IRtpDatabase db1, IRtpDatabase db2)
        {
            var dict = new Dictionary<string, List<string>>();
            var patientIds = db1.GetIdsOfAvailablePatients();

            foreach (var patientId in patientIds)
            {
                dict.Add(patientId, new List<string>());
                var courseIds = db1.GetIdsOfAvailableCourses(patientId);
                foreach (var courseId in courseIds)
                {
                    if (db2.CanAddCourseToPatient(courseId, patientId))
                    {
                        var result = db2.AddCourse(courseId, patientId);
                        OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs(result.Success, result.Info));
                        if (!result.Success) return new CopyResult(false, result.Info);
                        dict[patientId].Add(courseId);
                    }
                    else
                        OnProgressChanged?.Invoke(this,
                            new OnProgressChangedEventArgs(true, $"Skipped course with ID {courseId}."));
                }
            }

            var msg = dict.Select((key, value) => $"({key}: {string.Join(", ", value)})");
            return new CopyResult(true, $"Successfully copied courses {string.Join(", ", msg)}");
        }

        private CopyResult CopyStructureSets(IRtpDatabase db1, IRtpDatabase db2)
        {
            var dict = new Dictionary<string, List<string>>();
            var patientIds = db1.GetIdsOfAvailablePatients();

            foreach (var patientId in patientIds)
            {
                dict.Add(patientId, new List<string>());
                var structureSetIds = db1.GetIdsOfAvailableStructureSets(patientId);
                foreach (var structureSetId in structureSetIds)
                {
                    if (db2.CanAddStructureSetWithId(structureSetId, patientId))
                    {
                        var result = db2.AddStructureSet(structureSetId, patientId);
                        OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs(result.Success, result.Info));
                        if (!result.Success) return new CopyResult(false, result.Info);
                        dict[patientId].Add(structureSetId);
                    }
                    else
                        OnProgressChanged?.Invoke(this,
                            new OnProgressChangedEventArgs(true, $"Skipped structure set with ID {structureSetId}."));
                }
            }

            var msg = dict.Select((key, value) => $"({key}: {string.Join(", ", value)})");
            return new CopyResult(true, $"Successfully copied structure sets {string.Join(", ", msg)}");
        }

        private CopyResult CopyPatients(IRtpDatabase db1, IRtpDatabase db2)
        {
            var patientIds = db1.GetIdsOfAvailablePatients();
            foreach (var patientId in patientIds)
            {
                if (db2.CanAddPatientWithId(patientId))
                {
                    var patientName = db1.GetPatientName(patientId);
                    var addPatientResult = db2.AddPatient(patientId, patientName);
                    OnProgressChanged?.Invoke(this,
                        new OnProgressChangedEventArgs(addPatientResult.Success, addPatientResult.Info));
                    if (!addPatientResult.Success) return new CopyResult(false, addPatientResult.Info);
                }
                else
                    OnProgressChanged?.Invoke(this,
                        new OnProgressChangedEventArgs(true, $"Skipped patient {patientId}"));
            }

            return new CopyResult(true, $"Successfully copied patients with IDs {string.Join(", ", patientIds)}");
        }

        private CopyResult CopyOptimizationSummaries(IRtpDatabase db1, IRtpDatabase db2)
        {
            var optimizationSummaries = db1.GetAllOptimizationSummaries();
            foreach (var summary in optimizationSummaries)
                if (db2.CanAddOptimizationSummary(summary))
                {
                    var result = db2.AddOptimizationSummary(summary);
                    OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs(result.Success, result.Info));
                    if (!result.Success) return new CopyResult(result.Success, result.Info);
                }
                else
                {
                    OnProgressChanged?.Invoke(this,
                        new OnProgressChangedEventArgs(true, $"Skipp optimization summary {summary.Id}."));
                }

            return new CopyResult(true,
                $"Successfully copied optimization summaries with IDs " +
                $"{string.Join(", ", optimizationSummaries.Select(x => x.Id))}");
        }
    }

    class CopyResult
    {
        public bool Success { get; }
        public string Info { get; }

        public CopyResult(bool success, string info)
        {
            Success = success;
            Info = info;
        }

        protected bool Equals(CopyResult other)
        {
            return Success == other.Success && Info == other.Info;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((CopyResult)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Success, Info);
        }
    }
}
