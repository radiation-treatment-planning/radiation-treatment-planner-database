﻿using System;

namespace RadiationTreatmentPlanner.Database.SQLite
{
    public class OnProgressChangedEventArgs
    {
        public bool Success { get; }
        public string Info { get; }

        public OnProgressChangedEventArgs(bool success, string info)
        {
            Success = success;
            Info = info;
        }

        protected bool Equals(OnProgressChangedEventArgs other)
        {
            return Success == other.Success && Info == other.Info;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((OnProgressChangedEventArgs)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Success, Info);
        }
    }
}