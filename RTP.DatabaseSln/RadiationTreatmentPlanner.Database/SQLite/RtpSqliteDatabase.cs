﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Data.Sqlite;
using RadiationTreatmentPlanner.Database.Interface;
using RadiationTreatmentPlanner.Database.SQLite.Serialization;
using RadiationTreatmentPlanner.Optimization;
using RadiationTreatmentPlanner.Utils.Patient;

namespace RadiationTreatmentPlanner.Database.SQLite
{
    public class RtpSqliteDatabase : IRtpDatabase
    {
        private readonly RtpSerializer _serializer;
        public string ConnectionString { get; }
        public bool IsDatabaseConnected { get; private set; }

        private readonly SqliteConnection _connection;

        // Table IDs.
        public string PatientsTableId { get; } = "Patients";
        public string StructureSetsTableId { get; } = "StructureSets";
        public string CoursesTableId { get; } = "Courses";
        public string PlanSetupsTableId { get; } = "PlanSetups";
        public string OptimizationSummariesTableId { get; } = "OptimizationSummaries";
        public string OptimizationResultsTableId { get; } = "OptimizationResults";

        // Attribute IDs.
        public string PatientsTablePrimaryKey { get; } = "ID";
        public string PatientsTableNameId { get; } = "Name";

        public string StructureSetsTableStructureSetId { get; } = "ID";
        public string StructureSetsTablePatientId { get; } = "PatientId";

        public string CoursesTableCourseId { get; } = "ID";
        public string CoursesTablePatientId { get; } = "PatientId";

        public string PlanSetupsTablePlanSetupId { get; } = "ID";
        public string PlanSetupsTablePatientId { get; } = "PatientId";
        public string PlanSetupsTableCourseId { get; } = "CourseId";
        public string PlanSetupsTableStructureSetId { get; } = "StructureSetId";
        public string PlanSetupsTablePlanSetup { get; } = "PlanSetup";

        public string OptimizationSummariesTablePrimaryKey { get; } = "ID";
        public string OptimizationSummariesTableDescriptionId { get; } = "Description";
        public string OptimizationSummariesTableOptionalAdditionalInformationId { get; } = "OptionalAdditionalInformation";

        public string OptimizationResultsTablePrimaryKey { get; } = "ID";
        public string OptimizationResultsTablePatientId { get; } = "PatientId";
        public string OptimizationResultsTableCourseId { get; } = "CourseId";
        public string OptimizationResultsTablePlanSetupId { get; } = "PlanSetupId";
        public string OptimizationResultsTableStructureSetId { get; } = "StructureSetId";
        public string OptimizationResultsTableOptimizationRunResult { get; } = "OptimizationRunResult";
        public string OptimizationResultsTableOptimizationSummary { get; } = "OptimizationSummary";


        public RtpSqliteDatabase(FileInfo fullFilePath, RtpSerializer serializer)
        {
            _serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
            ConnectionString = CreateConnectionString(fullFilePath);
            _connection = new SqliteConnection(ConnectionString);
        }

        private string CreateConnectionString(FileInfo fullFilePath)
        {
            return new SqliteConnectionStringBuilder
            {
                DataSource = fullFilePath.FullName,
                Mode = SqliteOpenMode.ReadWriteCreate,
            }.ToString();
        }

        public OpenDbConnectionResult OpenConnection()
        {
            try
            {
                _connection.Open();
                IsDatabaseConnected = true;
                return new OpenDbConnectionResult(true, "");
            }
            catch (Exception e)
            {
                return new OpenDbConnectionResult(false, e.Message);
            }
        }

        public CloseDbConnectionResult CloseConnection()
        {
            try
            {
                _connection.Close();
                IsDatabaseConnected = false;
                return new CloseDbConnectionResult(true, "");
            }
            catch (Exception e)
            {
                return new CloseDbConnectionResult(false, e.Message);
            }
        }

        public RtpDbInitializationResult Initialize()
        {
            try
            {
                CreatePatientsTableIfNotExists();
                CreateStructureSetsTableIfNotExists();
                CreateCoursesTableIfNotExists();
                CreatePlanSetupsTableIfNotExists();
                CreateOptimizationSummariesTableIfNotExists();
                CreateOptimizationResultsTableIfNotExists();
                return new RtpDbInitializationResult(true, "");
            }
            catch (Exception e)
            {
                return new RtpDbInitializationResult(false, e.Message);
            }
        }

        private void CreateOptimizationSummariesTableIfNotExists()
        {
            var command = _connection.CreateCommand();
            command.CommandText =
                $"CREATE TABLE IF NOT EXISTS '{OptimizationSummariesTableId}' (" +
                $"'{OptimizationSummariesTablePrimaryKey}' TEXT NOT NULL, " +
                $"'{OptimizationSummariesTableDescriptionId}' TEXT NOT NULL DEFAULT 'N/A', " +
                $"'{OptimizationSummariesTableOptionalAdditionalInformationId}' TEXT NOT NULL DEFAULT 'N/A', " +
                $"PRIMARY KEY('{OptimizationSummariesTablePrimaryKey}'), " +
                $"UNIQUE('{OptimizationSummariesTablePrimaryKey}')" +
                $");";
            command.ExecuteNonQuery();
        }

        private void CreateOptimizationResultsTableIfNotExists()
        {
            var command = _connection.CreateCommand();
            command.CommandText =
                $"CREATE TABLE IF NOT EXISTS '{OptimizationResultsTableId}' (" +
                $"'{OptimizationResultsTablePrimaryKey}' INTEGER NOT NULL, " +
                $"'{OptimizationResultsTablePatientId}' TEXT NOT NULL, " +
                $"'{OptimizationResultsTableCourseId}' TEXT NOT NULL, " +
                $"'{OptimizationResultsTablePlanSetupId}' TEXT NOT NULL, " +
                $"'{OptimizationResultsTableStructureSetId}' TEXT NOT NULL, " +
                $"'{OptimizationResultsTableOptimizationRunResult}' TEXT NOT NULL, " +
                $"'{OptimizationResultsTableOptimizationSummary}' TEXT NOT NULL, " +
                $"PRIMARY KEY('{OptimizationResultsTablePrimaryKey}' AUTOINCREMENT), " +
                $"UNIQUE('{OptimizationResultsTablePrimaryKey}'), " +
                $"FOREIGN KEY('{OptimizationResultsTablePatientId}', '{OptimizationResultsTableCourseId}', '{OptimizationResultsTablePlanSetupId}', '{OptimizationResultsTableStructureSetId}') " +
                $"REFERENCES '{PlanSetupsTableId}'('{PlanSetupsTablePatientId}', '{PlanSetupsTableCourseId}', '{PlanSetupsTablePlanSetupId}', '{PlanSetupsTableStructureSetId}') ON DELETE CASCADE " +
                $"FOREIGN KEY('{OptimizationResultsTableOptimizationSummary}') " +
                $"REFERENCES '{OptimizationSummariesTableId}'('{OptimizationSummariesTablePrimaryKey}') ON DELETE CASCADE" +
                $");";
            command.ExecuteNonQuery();
        }

        public bool CanAddPatientWithId(string id)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT COUNT(*) " +
                    $"FROM '{PatientsTableId}' " +
                    $"WHERE {PatientsTablePrimaryKey} = '{id}';";

                var rowCount = Convert.ToInt32(command.ExecuteScalar());
                return rowCount == 0;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool CanAddStructureSetWithId(string structureSetId, string patientId)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT COUNT(*) " +
                    $"FROM '{StructureSetsTableId}' " +
                    $"WHERE {StructureSetsTableStructureSetId} = '{structureSetId}' " +
                    $"AND " +
                    $"{StructureSetsTablePatientId} = '{patientId}';";

                var rowCount = Convert.ToInt32(command.ExecuteScalar());
                return rowCount == 0;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private void CreatePlanSetupsTableIfNotExists()
        {
            var command = _connection.CreateCommand();
            command.CommandText =
                $"CREATE TABLE IF NOT EXISTS '{PlanSetupsTableId}' (" +
                $"'{PlanSetupsTablePlanSetupId}' TEXT NOT NULL, " +
                $"'{PlanSetupsTablePatientId}' TEXT NOT NULL, " +
                $"'{PlanSetupsTableCourseId}' TEXT NOT NULL, " +
                $"'{PlanSetupsTableStructureSetId}' TEXT NOT NULL, " +
                $"'{PlanSetupsTablePlanSetup}' TEXT NOT NULL, " +
                $"PRIMARY KEY('{PlanSetupsTablePlanSetupId}', '{PlanSetupsTablePatientId}', '{PlanSetupsTableCourseId}', '{PlanSetupsTableStructureSetId}'), " +
                $"UNIQUE('{PlanSetupsTablePlanSetupId}', '{PlanSetupsTablePatientId}', '{PlanSetupsTableCourseId}', '{PlanSetupsTableStructureSetId}'), " +
                $"FOREIGN KEY('{PlanSetupsTableStructureSetId}', '{PlanSetupsTablePatientId}') REFERENCES '{StructureSetsTableId}'('{StructureSetsTableStructureSetId}', '{StructureSetsTablePatientId}') ON DELETE CASCADE, " +
                $"FOREIGN KEY('{PlanSetupsTableCourseId}', '{PlanSetupsTablePatientId}') REFERENCES '{CoursesTableId}'('{CoursesTableCourseId}', '{CoursesTablePatientId}') ON DELETE CASCADE" +
                $");";
            command.ExecuteNonQuery();
        }

        private void CreateCoursesTableIfNotExists()
        {
            var command = _connection.CreateCommand();
            command.CommandText =
                $"CREATE TABLE IF NOT EXISTS '{CoursesTableId}'(" +
                $"'{CoursesTableCourseId}' TEXT NOT NULL, " +
                $"'{CoursesTablePatientId}' TEXT NOT NULL, " +
                $"UNIQUE('{CoursesTableCourseId}', '{CoursesTablePatientId}'), " +
                $"PRIMARY KEY('{CoursesTableCourseId}', '{CoursesTablePatientId}'), " +
                $"FOREIGN KEY('{CoursesTablePatientId}') REFERENCES '{PatientsTableId}'('{PatientsTablePrimaryKey}') " +
                $"ON DELETE CASCADE" +
                ");";
            command.ExecuteNonQuery();
        }

        private void CreateStructureSetsTableIfNotExists()
        {
            var command = _connection.CreateCommand();
            command.CommandText =
                $"CREATE TABLE IF NOT EXISTS '{StructureSetsTableId}'(" +
                $"'{StructureSetsTableStructureSetId}' TEXT NOT NULL, " +
                $"'{StructureSetsTablePatientId}' TEXT NOT NULL, " +
                $"UNIQUE({StructureSetsTableStructureSetId}, {StructureSetsTablePatientId}), " +
                $"PRIMARY KEY('{StructureSetsTableStructureSetId}', '{StructureSetsTablePatientId}'), " +
                $"FOREIGN KEY('{StructureSetsTablePatientId}') REFERENCES '{PatientsTableId}'('{PatientsTablePrimaryKey}') " +
                $"ON DELETE CASCADE" +
                ");";
            command.ExecuteNonQuery();
        }

        private void CreatePatientsTableIfNotExists()
        {
            var command = _connection.CreateCommand();
            command.CommandText =
                $"CREATE TABLE IF NOT EXISTS '{PatientsTableId}' " +
                $"('{PatientsTablePrimaryKey}' TEXT NOT NULL UNIQUE, " +
                $"'{PatientsTableNameId}' TEXT NOT NULL, " +
                $"PRIMARY KEY('{PatientsTablePrimaryKey}'));";
            command.ExecuteNonQuery();
        }


        public DatabaseOperationExecutionResult AddPatient(string id, string name)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"INSERT INTO '{PatientsTableId}'({PatientsTablePrimaryKey}, {PatientsTableNameId}) " +
                    $"VALUES ('{id}', '{name}');";
                command.ExecuteNonQuery();
                var info = $"Added patient with ID {id} and name {name}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }

        public DatabaseOperationExecutionResult DeletePatient(string id)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"DELETE FROM '{PatientsTableId}' WHERE {PatientsTablePrimaryKey} = '{id}';";
                command.ExecuteNonQuery();
                var info = $"Deleted patient with ID {id}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }
        public string GetPatientName(string patientId)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT {PatientsTableNameId} " +
                    $"FROM '{PatientsTableId}' " +
                    $"WHERE {PatientsTablePrimaryKey} = '{patientId}';";

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var name = reader.GetString(0);
                        return name;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public IEnumerable<string> GetIdsOfAvailablePatients()
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT {PatientsTablePrimaryKey} " +
                    $"FROM '{PatientsTableId}';";

                var patientIds = new List<string>();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = reader.GetString(0);
                        patientIds.Add(id);
                    }
                }
                return patientIds;
            }
            catch (Exception e)
            {
                return new List<string>(0);
            }
        }

        public DatabaseOperationExecutionResult AddStructureSet(string structureSetId, string patientId)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"INSERT INTO '{StructureSetsTableId}'({StructureSetsTableStructureSetId}, {StructureSetsTablePatientId}) " +
                    $"VALUES ('{structureSetId}', '{patientId}');";
                command.ExecuteNonQuery();
                var info = $"Added structure set with ID {structureSetId} for patient with ID {patientId}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }

        public DatabaseOperationExecutionResult DeleteStructureSet(string structureSetId, string patientId)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"DELETE FROM '{StructureSetsTableId}' " +
                    $"WHERE {StructureSetsTableStructureSetId} = '{structureSetId}' " +
                    $"AND " +
                    $"{StructureSetsTablePatientId} = '{patientId}';";
                command.ExecuteNonQuery();
                var info = $"Deleted structure set with ID {structureSetId} for patient with ID {patientId}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }

        public IEnumerable<string> GetIdsOfAvailableStructureSets(string patientId)
        {
            if (patientId == null) throw new ArgumentNullException(nameof(patientId));
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT {StructureSetsTableStructureSetId} " +
                    $"FROM '{StructureSetsTableId}'" +
                    $"WHERE {StructureSetsTablePatientId} = '{patientId}';";

                var structureSetIds = new List<string>();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = reader.GetString(0);
                        structureSetIds.Add(id);
                    }
                }
                return structureSetIds;
            }
            catch (Exception e)
            {
                return new List<string>(0);
            }
        }

        public DatabaseOperationExecutionResult AddCourse(string courseId, string patientId)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"INSERT INTO '{CoursesTableId}'({CoursesTableCourseId}, {CoursesTablePatientId}) " +
                    $"VALUES ('{courseId}', '{patientId}');";
                command.ExecuteNonQuery();
                var info = $"Added course with ID {courseId} for patient with ID {patientId}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }

        public DatabaseOperationExecutionResult DeleteCourse(string courseId, string patientId)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"DELETE FROM '{CoursesTableId}' " +
                    $"WHERE {CoursesTableCourseId} = '{courseId}' " +
                    $"AND " +
                    $"{CoursesTablePatientId} = '{patientId}';";
                command.ExecuteNonQuery();
                var info = $"Deleted course with ID {courseId} for patient with ID {patientId}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }

        public bool CanAddCourseToPatient(string courseId, string patientId)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT COUNT(*) " +
                    $"FROM '{PatientsTableId}' " +
                    $"WHERE {PatientsTablePrimaryKey} = '{patientId}';";

                var rowCount = Convert.ToInt32(command.ExecuteScalar());
                var doesPatientExist = rowCount > 0;
                if (!doesPatientExist) return false;

                command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT COUNT(*) " +
                    $"FROM '{CoursesTableId}' " +
                    $"WHERE {CoursesTableCourseId} = '{courseId}' " +
                    $"AND " +
                    $"{CoursesTablePatientId} = '{patientId}';";
                rowCount = Convert.ToInt32(command.ExecuteScalar());
                var doesCourseAlreadyExist = rowCount > 0;
                return !doesCourseAlreadyExist;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<string> GetIdsOfAvailableCourses(string patientId)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT {CoursesTableCourseId} " +
                    $"FROM '{CoursesTableId}'" +
                    $"WHERE {CoursesTablePatientId} = '{patientId}';";

                var courseIds = new List<string>();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = reader.GetString(0);
                        courseIds.Add(id);
                    }
                }
                return courseIds;
            }
            catch (Exception e)
            {
                return new List<string>(0);
            }
        }

        public DatabaseOperationExecutionResult AddPlanSetup(UPlanSetup planSetup)
        {
            try
            {
                var planSetupId = planSetup.Id;
                var courseId = planSetup.Course.Id;
                var patientId = planSetup.Patient.Id;
                var structureSetId = planSetup.StructureSet.Id;

                var serializedPlanSetup = _serializer.Serialize(planSetup.Detach());

                var command = _connection.CreateCommand();
                var commandText =
                    $"INSERT INTO '{PlanSetupsTableId}'({PlanSetupsTablePlanSetupId}, {PlanSetupsTableCourseId}, {PlanSetupsTablePatientId}," +
                    $" {PlanSetupsTableStructureSetId}, {PlanSetupsTablePlanSetup}) " +
                    $"VALUES ('{planSetupId}', '{courseId}', '{patientId}', '{structureSetId}', '{serializedPlanSetup}');";

                command.CommandText = commandText;
                command.ExecuteNonQuery();
                var info = $"Added Plan setup with ID {planSetupId} for patient with ID {patientId}, " +
                           $"course with ID {courseId} and structure set with ID {structureSetId}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }

        public DatabaseOperationExecutionResult AddPlanSetup(UPlanSetupDetached planSetup, string patientId, string courseId)
        {
            try
            {
                var planSetupId = planSetup.Id;
                var structureSetId = planSetup.StructureSet.Id;

                var serializedPlanSetup = _serializer.Serialize(planSetup);

                var command = _connection.CreateCommand();
                var commandText =
                    $"INSERT INTO '{PlanSetupsTableId}'({PlanSetupsTablePlanSetupId}, {PlanSetupsTableCourseId}, {PlanSetupsTablePatientId}," +
                    $" {PlanSetupsTableStructureSetId}, {PlanSetupsTablePlanSetup}) " +
                    $"VALUES ('{planSetupId}', '{courseId}', '{patientId}', '{structureSetId}', '{serializedPlanSetup}');";

                command.CommandText = commandText;
                command.ExecuteNonQuery();
                var info = $"Added Plan setup with ID {planSetupId} for patient with ID {patientId}, " +
                           $"course with ID {courseId} and structure set with ID {structureSetId}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }

        public DatabaseOperationExecutionResult DeletePlanSetup(string planSetupId, string courseId, string structureSetId,
            string patientId)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"DELETE FROM '{PlanSetupsTableId}' " +
                    $"WHERE {PlanSetupsTablePlanSetupId} = '{planSetupId}' " +
                    $"AND {PlanSetupsTableCourseId} = '{courseId}' " +
                    $"AND {PlanSetupsTablePatientId} = '{patientId}' " +
                    $"AND {PlanSetupsTableStructureSetId} = '{structureSetId}';";
                command.ExecuteNonQuery();
                var info = $"Deleted plan setup with ID {planSetupId} for patient with ID {patientId}, " +
                           $"course with ID {courseId} and structure set with ID {structureSetId}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }

        public UPlanSetupDetached GetPlanSetupOrDefault(string planSetupId, string courseId, string structureSetId, string patientId)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT * " +
                    $"FROM '{PlanSetupsTableId}' " +
                    $"WHERE {PlanSetupsTablePlanSetupId} = '{planSetupId}' " +
                    $"AND {PlanSetupsTableCourseId} = '{courseId}' " +
                    $"AND {PlanSetupsTablePatientId} = '{patientId}' " +
                    $"AND {PlanSetupsTableStructureSetId} = '{structureSetId}';";

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var serializedPlanSetup = reader.GetString(4);
                        return _serializer.DeserializePlanSetup(serializedPlanSetup);
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public IEnumerable<UPlanSetupDetached> GetAllPlanSetups(string patientId, string courseId)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT * " +
                    $"FROM '{PlanSetupsTableId}' " +
                    $"WHERE {PlanSetupsTableCourseId} = '{courseId}' " +
                    $"AND {PlanSetupsTablePatientId} = '{patientId}';";

                var planSetups = new List<UPlanSetupDetached>();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var serializedPlanSetup = reader.GetString(4);
                        var planSetup = _serializer.DeserializePlanSetup(serializedPlanSetup);
                        planSetups.Add(planSetup);
                    }
                }

                return planSetups;
            }
            catch (Exception e)
            {
                return new List<UPlanSetupDetached>(0);
            }
        }

        public bool CanAddOptimizationSummary(RtpOptimizationSummary optimizationSummary)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT COUNT(*) " +
                    $"FROM '{OptimizationSummariesTableId}' " +
                    $"WHERE {OptimizationSummariesTablePrimaryKey} = '{optimizationSummary.Id}';";

                var rowCount = Convert.ToInt32(command.ExecuteScalar());
                var doesOptimizationSummaryExist = rowCount > 0;
                return !doesOptimizationSummaryExist;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public DatabaseOperationExecutionResult AddOptimizationSummary(RtpOptimizationSummary optimizationSummary)
        {
            try
            {
                var command = _connection.CreateCommand();
                var commandText =
                    $"INSERT INTO '{OptimizationSummariesTableId}'({OptimizationSummariesTablePrimaryKey}, " +
                    $"{OptimizationSummariesTableDescriptionId}, {OptimizationSummariesTableOptionalAdditionalInformationId}) " +
                    $"VALUES ('{optimizationSummary.Id}', '{optimizationSummary.Description}', '{optimizationSummary.OptionalAdditionalInformation}');";

                command.CommandText = commandText;
                command.ExecuteNonQuery();
                var info = $"Added optimization summary with ID {optimizationSummary.Id}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }

        public DatabaseOperationExecutionResult DeleteOptimizationSummaryWithId(string id)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"DELETE " +
                    $"FROM '{OptimizationSummariesTableId}' " +
                    $"WHERE {OptimizationSummariesTablePrimaryKey} = '{id}';";

                command.ExecuteNonQuery();
                var info = $"Deleted optimization summary with ID {id}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }

        public RtpOptimizationSummary GetOptimizationSummaryWithId(string id)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT * " +
                    $"FROM '{OptimizationSummariesTableId}' " +
                    $"WHERE {OptimizationSummariesTablePrimaryKey} = '{id}';";

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var optimizationSummaryId = reader.GetString(0);
                        var optimizationSummaryDescription = reader.GetString(1);
                        var optimizationSummaryAdditionalInformation = reader.GetString(2);
                        return new RtpOptimizationSummary(optimizationSummaryId, optimizationSummaryDescription,
                            optimizationSummaryAdditionalInformation);
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<RtpOptimizationSummary> GetAllOptimizationSummaries()
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"SELECT * " +
                    $"FROM '{OptimizationSummariesTableId}';";

                var optimizationSummaries = new List<RtpOptimizationSummary>();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var optimizationSummaryId = reader.GetString(0);
                        var optimizationSummaryDescription = reader.GetString(1);
                        var optimizationSummaryAdditionalInformation = reader.GetString(2);
                        var optimizationSummary = new RtpOptimizationSummary(optimizationSummaryId, optimizationSummaryDescription,
                            optimizationSummaryAdditionalInformation);
                        optimizationSummaries.Add(optimizationSummary);
                    }
                }

                return optimizationSummaries;
            }
            catch (Exception e)
            {
                return new List<RtpOptimizationSummary>();
            }
        }

        public DatabaseOperationExecutionResult AddOptimizationResult(RtpOptimizationRunResult optimizationRunResult,
            string optimizationSummaryId)
        {
            try
            {
                var planSetupId = optimizationRunResult.PlanSetupId;
                var courseId = optimizationRunResult.CourseId;
                var patientId = optimizationRunResult.PatientId;
                var structureSetId = optimizationRunResult.StructureSetId;

                var serializedOptimizationRunResult = _serializer.Serialize(optimizationRunResult);

                var command = _connection.CreateCommand();
                var commandText =
                    $"INSERT INTO '{OptimizationResultsTableId}'({OptimizationResultsTablePatientId}, {OptimizationResultsTableCourseId}, " +
                    $"{OptimizationResultsTablePlanSetupId}, {OptimizationResultsTableStructureSetId}, {OptimizationResultsTableOptimizationRunResult}, " +
                    $"{OptimizationResultsTableOptimizationSummary}) " +
                    $"VALUES ('{patientId}', '{courseId}', '{planSetupId}', '{structureSetId}', " +
                    $"'{serializedOptimizationRunResult}', '{optimizationSummaryId}');";

                command.CommandText = commandText;
                command.ExecuteNonQuery();
                var info = $"Added optimization result for patient with ID {patientId}, course with ID {courseId}, " +
                           $"plan setup with ID {planSetupId} and structure set with ID {structureSetId}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }

        public RtpOptimizationRunResult GetOptimizationResultOrDefault(int id)
        {
            var command = _connection.CreateCommand();
            command.CommandText =
                $"SELECT * " +
                $"FROM '{OptimizationResultsTableId}' " +
                $"WHERE {OptimizationResultsTablePrimaryKey} = '{id}';";

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var serializedOptimizationRunResult = reader.GetString(5);
                    var optimizationResult = _serializer.DeserializeOptimizationRunResult(serializedOptimizationRunResult);
                    return optimizationResult;
                }
            }

            return null;
        }

        public List<RtpOptimizationRunResult> GetOptimizationResultsOfOptimizationSummaryWithId(string id)
        {
            var command = _connection.CreateCommand();
            command.CommandText =
                $"SELECT * " +
                $"FROM '{OptimizationResultsTableId}' " +
                $"WHERE {OptimizationResultsTableOptimizationSummary} = '{id}';";

            var optimizationResults = new List<RtpOptimizationRunResult>();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var serializedOptimizationRunResult = reader.GetString(5);
                    var optimizationResult = _serializer.DeserializeOptimizationRunResult(serializedOptimizationRunResult);
                    optimizationResults.Add(optimizationResult);
                }
            }
            return optimizationResults;
        }

        public DatabaseOperationExecutionResult DeleteOptimizationResult(int id)
        {
            try
            {
                var command = _connection.CreateCommand();
                command.CommandText =
                    $"DELETE FROM '{OptimizationResultsTableId}' " +
                    $"WHERE {OptimizationResultsTablePrimaryKey} = '{id}';";
                command.ExecuteNonQuery();
                var info = $"Deleted optimization result with ID {id}.";
                return new DatabaseOperationExecutionResult(true, info);
            }
            catch (Exception e)
            {
                return new DatabaseOperationExecutionResult(false, e.Message);
            }
        }
    }
}
