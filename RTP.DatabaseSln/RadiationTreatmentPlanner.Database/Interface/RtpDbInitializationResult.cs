﻿using System;

namespace RadiationTreatmentPlanner.Database.Interface
{
    public class RtpDbInitializationResult
    {
        public bool Success { get; }
        public string Info { get; }

        /// <summary>
        /// Database initialization result.
        /// </summary>
        /// <param name="success">True, if initialization was successful, otherwise false.</param>
        /// <param name="info">Information about the initialization process.</param>
        public RtpDbInitializationResult(bool success, string info)
        {
            Success = success;
            Info = info ?? throw new ArgumentNullException(nameof(info));
        }

        protected bool Equals(RtpDbInitializationResult other)
        {
            return Success == other.Success && Info == other.Info;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((RtpDbInitializationResult)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Success, Info);
        }
    }
}
