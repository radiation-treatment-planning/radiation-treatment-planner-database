﻿using System;

namespace RadiationTreatmentPlanner.Database.Interface
{
    public class RtpOptimizationSummary
    {
        public string Id { get; }
        public string Description { get; }
        public string OptionalAdditionalInformation { get; }

        public RtpOptimizationSummary(string id, string description, string optionalAdditionalInformation)
        {
            Id = id ?? throw new ArgumentNullException(nameof(id));
            Description = description ?? throw new ArgumentNullException(nameof(description));
            OptionalAdditionalInformation = optionalAdditionalInformation ??
                                            throw new ArgumentNullException(nameof(optionalAdditionalInformation));
        }

        protected bool Equals(RtpOptimizationSummary other)
        {
            return Id == other.Id && Description == other.Description &&
                   Equals(OptionalAdditionalInformation, other.OptionalAdditionalInformation);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((RtpOptimizationSummary)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Description, OptionalAdditionalInformation);
        }
    }
}
