﻿using System.Collections.Generic;
using RadiationTreatmentPlanner.Optimization;
using RadiationTreatmentPlanner.Utils.Patient;

namespace RadiationTreatmentPlanner.Database.Interface
{
    public interface IRtpDatabase
    {
        bool IsDatabaseConnected { get; }
        OpenDbConnectionResult OpenConnection();
        CloseDbConnectionResult CloseConnection();
        RtpDbInitializationResult Initialize();

        // Patient methods.
        bool CanAddPatientWithId(string id);
        DatabaseOperationExecutionResult AddPatient(string id, string name);
        DatabaseOperationExecutionResult DeletePatient(string id);
        string GetPatientName(string patientId);
        IEnumerable<string> GetIdsOfAvailablePatients();

        // Structure Set methods.
        DatabaseOperationExecutionResult AddStructureSet(string structureSetId, string patientId);
        DatabaseOperationExecutionResult DeleteStructureSet(string structureSetId, string patientId);
        IEnumerable<string> GetIdsOfAvailableStructureSets(string patientId);
        bool CanAddStructureSetWithId(string structureSetId, string patientId);

        // Course methods.
        DatabaseOperationExecutionResult AddCourse(string courseId, string patientId);
        DatabaseOperationExecutionResult DeleteCourse(string courseId, string patientId);
        bool CanAddCourseToPatient(string courseId, string patientId);
        IEnumerable<string> GetIdsOfAvailableCourses(string patientId);

        // Plan Setup methods.
        DatabaseOperationExecutionResult AddPlanSetup(UPlanSetup planSetup);
        DatabaseOperationExecutionResult AddPlanSetup(UPlanSetupDetached planSetup, string patientId, string courseId);
        DatabaseOperationExecutionResult DeletePlanSetup(string planSetupId, string courseId, string structureSetId,
            string patientId);
        UPlanSetupDetached GetPlanSetupOrDefault(string planSetupId, string courseId, string structureSetId, string patientId);
        IEnumerable<UPlanSetupDetached> GetAllPlanSetups(string patientId, string courseId);

        // Optimization summary methods.
        bool CanAddOptimizationSummary(RtpOptimizationSummary optimizationSummary);
        DatabaseOperationExecutionResult AddOptimizationSummary(RtpOptimizationSummary optimizationSummary);
        DatabaseOperationExecutionResult DeleteOptimizationSummaryWithId(string id);
        RtpOptimizationSummary GetOptimizationSummaryWithId(string id);
        List<RtpOptimizationSummary> GetAllOptimizationSummaries();
        List<RtpOptimizationRunResult> GetOptimizationResultsOfOptimizationSummaryWithId(string id);

        // Optimization result methods.
        DatabaseOperationExecutionResult AddOptimizationResult(RtpOptimizationRunResult optimizationRunResult, string optimizationSummaryId);
        DatabaseOperationExecutionResult DeleteOptimizationResult(int id);
        RtpOptimizationRunResult GetOptimizationResultOrDefault(int id);
    }
}
