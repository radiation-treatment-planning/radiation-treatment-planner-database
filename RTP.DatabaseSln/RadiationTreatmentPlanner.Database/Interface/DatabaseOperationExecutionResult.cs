﻿using System;

namespace RadiationTreatmentPlanner.Database.Interface
{
    public class DatabaseOperationExecutionResult
    {
        public bool Success { get; }
        public string Info { get; }

        /// <summary>
        /// Database operation execution result.
        /// </summary>
        /// <param name="success">True, if operation was executed successfully, false otherwise.</param>
        /// <param name="info">Information about the operation execution.</param>
        public DatabaseOperationExecutionResult(bool success, string info)
        {
            Success = success;
            Info = info ?? throw new ArgumentNullException(nameof(info));
        }

        protected bool Equals(DatabaseOperationExecutionResult other)
        {
            return Success == other.Success && Info == other.Info;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DatabaseOperationExecutionResult)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Success, Info);
        }
    }
}
