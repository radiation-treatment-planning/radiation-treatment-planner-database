﻿using System;

namespace RadiationTreatmentPlanner.Database.Interface
{
    public class CloseDbConnectionResult
    {

        public bool Success { get; }
        public string Info { get; }

        /// <summary>
        /// Result of trying to close a database connection.
        /// </summary>
        /// <param name="success">True, if closing database connection was successful, otherwise False.</param>
        /// <param name="info">Information about connection closing failure, in case closing was not successful.</param>
        public CloseDbConnectionResult(bool success, string info)
        {
            Success = success;
            Info = info ?? throw new ArgumentNullException(nameof(info));
        }

        protected bool Equals(CloseDbConnectionResult other)
        {
            return Success == other.Success && Info == other.Info;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((CloseDbConnectionResult)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Success, Info);
        }
    }
}
