﻿using System;

namespace RadiationTreatmentPlanner.Database.Interface
{
    public class OpenDbConnectionResult
    {
        public bool Success { get; }
        public string Info { get; }

        /// <summary>
        /// Result of trying to open a database connection.
        /// </summary>
        /// <param name="success">True, if opening database connection was successful, otherwise False.</param>
        /// <param name="info">Information about connection opening failure, in case connecting was not successful.</param>
        public OpenDbConnectionResult(bool success, string info)
        {
            Success = success;
            Info = info ?? throw new ArgumentNullException(nameof(info));
        }

        protected bool Equals(OpenDbConnectionResult other)
        {
            return Success == other.Success && Info == other.Info;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((OpenDbConnectionResult)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Success, Info);
        }
    }
}
