﻿namespace RadiationTreatmentPlanner.Database.Interface
{
    public interface IDatabaseManager
    {
        DbCopyResult CopyLeftToRight(IRtpDatabase db1, IRtpDatabase db2);
    }
}
