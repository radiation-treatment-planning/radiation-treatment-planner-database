# radiation-treatment-planner-database
A database interface for storing patient data and optimization results for radiation treatment planning + a concrete SQLite implementation of the RTP database.

## Entity-Relationship Diagram
![image](RTP-Patients-Database-v2.drawio.png)

## Installation
For example download form [NuGet](https://www.nuget.org/packages/RadiationTreatmentPlanner.Database/).

## Usage
Look at test project for examples.